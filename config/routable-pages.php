<?php

return [


    /**
     * if true, when loading page router routes, any page router class not found will be skipped
     */
    'ignore_page_router_class_not_found_errors' => false,

    /**
     * if true, when loading page router routes, any page router classes that fail to implement the PageType contract will be skipped
     */
    'ignore_page_router_interface_errors' => false,

    /**
     * request key that page Model will be assigned
     */
    'request_page_key' => 'page_model',

    'page_routers_meta' => [

//        'articles' => [
//            'display_name'     => 'Articles',
//            'page_router_class'  => Articles::class,
//            'admin_controller' => null,
//        ]
    ]
];
