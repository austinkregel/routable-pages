<?php


namespace UnstoppableCarl\RoutablePages;


use Illuminate\Config\Repository;
use Illuminate\Routing\Router;
use \UnstoppableCarl\RoutablePages\Contracts\PageRouter as PageRouterContract;
use UnstoppableCarl\RoutablePages\Middleware\PageInjector;

class PageRouter implements PageRouterContract{

    /**
     * key used to get page model from request via $request->get($key);
     * if no value set in class, use config value from 'page-types.request_page_key'
     * @var string
     */
    protected $requestPageKey;

    /**
     * controller namespace used for all routes bound by this page type
     * @var string
     */
    protected $controllerNamespace = 'App\Http\Controllers';

    /**
     * PageType constructor.
     * @param Repository $config
     */
    public function __construct(Repository $config) {
        $this->requestPageKey = $this->requestPageKey ?: $config->get('routable-pages.request_page_key', 'page_model');
    }

    public function bindPageRouteGroup(Router $router, $pageId, $path) {

        $attrs = $this->routeGroupAttributes($path, $pageId);

        $router->group($attrs, function ($router) {

            $this->bindPageRoutes($router);

        });
    }

    protected function routeGroupAttributes($path, $pageId) {

        $key = $this->requestPageKey;
        $injector = PageInjector::class . ':' . $key . ',' . $pageId;

        return [
            'as'        => $this->routeGroupName($pageId),
            'prefix'    => $path,
            'namespace' => $this->controllerNamespace,
            'middleware' => [
                $injector
            ],
        ];
    }

    protected function routeGroupName($pageId){
        return 'page_id_' . $pageId;
    }

    /**
     * bind page type routes here
     * @param Router $router
     */
    protected function bindPageRoutes(Router $router) {

        /**
         *  bind page sub routes here
         *  example:
         *  $router->get('/', 'Articles@all');
         *  $router->get('/{article}', 'Articles@single');
         */
    }


}
