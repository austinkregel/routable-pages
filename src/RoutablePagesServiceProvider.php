<?php

namespace UnstoppableCarl\RoutablePages;

use Schema;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Routing\Router;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use UnstoppableCarl\RoutablePages\Contracts\PageRepository as PageRepoContract;
use UnstoppableCarl\RoutablePages\Contracts\PageRouter as PageRouterContract;
use UnstoppableCarl\RoutablePages\Exceptions\PageRouterNotFoundException;
use UnstoppableCarl\RoutablePages\Middleware\PageInjector;
use UnstoppableCarl\RoutablePages\Repositories\PageRepository;

class RoutablePagesServiceProvider extends ServiceProvider {

    protected $configKey = 'routable-pages';

    public function register() {
        $this->registerConfig();

        $this->publishes([
            __DIR__ . '/../migrations/' => database_path('migrations')
        ], 'migrations');

        $this->app->bind(PageRepoContract::class, PageRepository::class);
    }

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot(Router $router,  Kernel $kernel) {


//        dd(get_class($kernel));
//        $kernel->pushMiddleware(PageInjector::class);
        if(!$this->app->routesAreCached()) {

//            $router->middleware('routable_pages.page_injector', PageInjector::class);
            if(Schema::hasTable('pages'))
                $this->bootRoutes($router);
        }
    }

    protected function registerConfig() {

        $key      = $this->configKey; // 'config-name'
        $fileName = $key . '.php'; // 'config-name.php'
        $filePath = __DIR__ . '/../config/' . $fileName;

        $this->publishes([
            $filePath => config_path($fileName),
        ]);

        $this->mergeConfigFrom($filePath, $key);
    }

    protected function bootRoutes(Router $router) {

        $pageRepo = $this->app->make(PageRepoContract::class);

        foreach($pageRepo->getRouteData() as $page) {

            $pageId          = Arr::get($page, 'id');
            $path            = Arr::get($page, 'path');
            $PageRouterClass = Arr::get($page, 'page_router_class');
            $pageType        = $this->getPageRouter($PageRouterClass, $pageId, $path);

            if(!$pageType) {
                continue;
            }

            $this->bindPageTypeRouteGroup($pageType, $router, $pageId, $path);

        }
    }

    /**
     * @param string $PageRouterClass
     * @param int    $pageId
     * @param string $path
     * @return bool|PageRouterContract
     * @throws PageRouterNotFoundException
     */
    protected function getPageRouter($PageRouterClass, $pageId, $path) {
        $ignorePageTypeNotFound     = $this->packageConfig('ignore_page_router_class_not_found_errors');
        $ignoreImplementationErrors = $this->packageConfig('ignore_page_router_interface_errors');

        if(!class_exists($PageRouterClass) && !isset($this->app[$PageRouterClass])) {
            if($ignorePageTypeNotFound) {
                return false;
            }
            else {
                throw new PageRouterNotFoundException($PageRouterClass, $pageId, $path);
            }
        }

        $pageType = $this->app->make($PageRouterClass);

        $implements = class_implements($pageType, PageRouterContract::class);
        if(!$implements && $ignoreImplementationErrors) {
            return false;
        }

        return $pageType;
    }

    protected function bindPageTypeRouteGroup(PageRouterContract $pageType, Router $router, $pageId, $path) {
        $pageType->bindPageRouteGroup($router, $pageId, $path);
    }

    protected function packageConfig($key = null, $default = null) {
        return $this->app['config']->get($this->configKey . '.' . $key, $default);
    }

}
