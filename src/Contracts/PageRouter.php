<?php


namespace UnstoppableCarl\RoutablePages\Contracts;


use Illuminate\Routing\Router;

interface PageRouter {

    public function bindPageRouteGroup(Router $router, $pageId, $path);
}
