<?php


namespace UnstoppableCarl\RoutablePages\Contracts;

interface PageRepository {

    public function findById($id);

    /**
     * must return array of page data arrays ['id' => null, 'path' => null, 'page_type_class']
     * @return mixed
     */
    public function getRouteData();
}
