<?php

namespace UnstoppableCarl\RoutablePages\Middleware;

use Closure;
use UnstoppableCarl\RoutablePages\Repositories\PageRepository;

class PageInjector {

    protected $pageRepository;

    public function __construct(PageRepository $pageRepository) {
        $this->pageRepository = $pageRepository;
    }

    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next, $key, $pageId) {

        $page = $this->pageRepository->findById($pageId);

        if(!$page) {
            abort(404);
        }
        $request->merge([$key => $page]);

        return $next($request);
    }

}
