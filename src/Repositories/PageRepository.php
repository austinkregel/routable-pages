<?php


namespace UnstoppableCarl\RoutablePages\Repositories;

use UnstoppableCarl\RoutablePages\Models\Page;
use UnstoppableCarl\RoutablePages\Contracts\PageRepository as PageRepoContract;

class PageRepository implements PageRepoContract {

    public function findById($id){
        return Page::find($id);
    }

    public function getRouteData() {
        return Page::query()
-                  ->select(['id', 'path', 'page_router_class'])
                   ->orderBy('route_priority', 'desc')
                   ->get()->toArray();

    }
}
