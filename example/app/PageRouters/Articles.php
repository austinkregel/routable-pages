<?php


namespace App\PageRouters;


use Illuminate\Routing\Router;
use UnstoppableCarl\RoutablePages\PageRouter;

class Articles extends PageRouter {

    protected function bindPageRoutes(Router $router) {
        $router->any('/', 'Articles@all');
        $router->any('/{article}', 'Articles@single');
    }

}
