<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class Articles extends Controller {

    public function all(Request $request){

        $page = $request->get('page_model');

        $out = 'Articles List';

        $out .= '<br>';

        $out .= $page->content;

        return $out;

    }

    public function single(Request $request) {

        $article = $request->route('article');

        $page = $request->get('page_model');

        $out = 'Single Article';

        $out .= '<br>';

        $out .= $page->content;

        $out .= '<br>';

        $out .= $article;

        return $out;

    }
}
